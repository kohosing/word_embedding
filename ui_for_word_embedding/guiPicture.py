import tkinter as tk
from tkinter import *
from tkinter import filedialog
from PIL import ImageTk, Image
from tkinter import PhotoImage
from skimage import io

import requests
import warnings
import json
import os

root = tk.Tk()
root.title('Model')
root.geometry('{}x{}'.format(800, 500))
root.resizable(0,0)
root.configure(background='white')

user_input = tk.StringVar(root)
serverip = 'http://10.10.1.182:3000'

def UploadFile1():
     global filename1

     filename1 = filedialog.askopenfilename()

def UploadFile2(event=None):
     global filename2

     filename2 = filedialog.askopenfilename()

def UploadAPI():
     global filename1
     global listbox

     url = f'{serverip}/create?name={user_input.get()}'
     files = [('file', open(f'{filename1}', 'rb')), ('file', open(f'{filename2}', 'rb'))]
     sendRequest = requests.post(url, files=files)
def saveResult(event=None):
     print('Selected: saveResult button')

def onselect(event):
     global picture
     global photoImg
     global img

     width = 400
     height = 400
     
     w = event.widget
     idx = int(w.curselection()[0])
     value = w.get(idx)

     cwd = os.getcwd()
     picture = requests.get(f'{serverip}/get_result?name={value}')
     with open(f'{cwd}/temp.gif', 'wb') as f:
          f.write(picture.content)

     warnings.simplefilter('ignore', Image.DecompressionBombWarning)
     cwd = os.getcwd()
     completeName = os.path.join(cwd, f'temp.gif')

     img = Image.open(completeName)
     img = img.resize((width,height), Image.ANTIALIAS)
     photoImg =  ImageTk.PhotoImage(img)
     
     picture=Label(root,image = photoImg)
     picture.grid(row=1, column=2, sticky="nsew")

def populatebox():
     listbox.delete('0','end')
     request_get_folder_list = requests.get(f'{serverip}/get_folder_list')
     response_data_list = request_get_folder_list.json()
     
     for i in response_data_list['folder_name']:
          listbox.insert("end", i)

# create all of the main containers
top_frame = Frame(root, bg='white', width=450, height=60, pady=3)
center = Frame(root, bg='white', width=50, height=40, padx=3, pady=3)
btm_frame = Frame(root, bg='white', width=450, height=45, pady=3)
btm_frame2 = Frame(root, bg='white', width=450, height=60, pady=3)

# layout all of the main containers
root.grid_rowconfigure(1, weight=1)
root.grid_columnconfigure(0, weight=1)

top_frame.grid(row=0, sticky="ew")
center.grid(row=1, sticky="nsew")
btm_frame.grid(row=3, sticky="ew")
btm_frame2.grid(row=4, sticky="ew")

label1 = Label(top_frame, text='Name:')
entry1 = Entry(top_frame, textvariable=user_input, background="grey")
button1 = Button(top_frame, text='File1(.txt file)', command=UploadFile1)
button2 = Button(top_frame, text='File2(.txt file)', command=UploadFile2)
button3 = Button(top_frame, text='Upload', command=UploadAPI)

# layout the widgets in the top frame
label1.grid(row=1, column=0)
entry1.grid(row=1, column=1)
button1.grid(row=2, column=0)
button2.grid(row=3, column=0)
button3.grid(row=2, column=1)

# create the center widgets
center.grid_rowconfigure(0, weight=1)
center.grid_columnconfigure(1, weight=1)

global listbox
global picture
global photoImg
global img

result = {}
width = 400
height = 400

request_get_folder_list = requests.get(f'{serverip}/get_folder_list')
response_data_list = request_get_folder_list.json()
listbox = Listbox(root, width=80, height=23)
listbox.grid(row=1, column=0, sticky='ns')
yscroll = Scrollbar(command=listbox.yview, orient=VERTICAL)
yscroll.grid(row=1, column=1, sticky='ns')
listbox.configure(yscrollcommand=yscroll.set)
for item in response_data_list['folder_name']:
    listbox.insert('end', item)

name_of_selection_from_box = listbox.bind('<<ListboxSelect>>', onselect)

button4 = Button(text='Save', command=saveResult)
buttonRefresh = Button(text="Update list", command = lambda: populatebox())

warnings.simplefilter('ignore', Image.DecompressionBombWarning)
cwd = os.getcwd()
completeName = os.path.join(cwd, f'temp.gif')
img = Image.open(completeName)
img = img.resize((width,height), Image.ANTIALIAS)
photoImg =  ImageTk.PhotoImage(img)

picture=Label(root,image = photoImg)
picture.grid(row=1, column=2, sticky="nsew")

btm_frame.grid_rowconfigure(0, weight=1)
btm_frame.grid_columnconfigure(1, weight=1)

buttonRefresh.grid(row=1, column=3, sticky="ns")
button4.grid(row=2, column=1, sticky="ns")

root.mainloop()