# -*- coding: UTF-8 -*-
import sys
import os
import json
 
from flask import Flask, request, jsonify, send_from_directory
from flask_cors import CORS
from werkzeug.utils import secure_filename
from waitress import serve
from include.Logger import logging, get_logger
import jieba
import numpy as np
import tensorflow.compat.v1 as tf
from W2V import *
from threading import Thread
from zipfile import ZipFile
import configparser

config = configparser.ConfigParser()
dir_name = os.path.basename(os.getcwd()).lower()
config.read(f'{dir_name}.conf')
threads = config['service']['threads']
port = config['service'].getint('port')
cleanup_interval = config['service']['cleanup_interval']
channel_timeout = config['service']['channel_timeout']
max_upload_size = config['service']['max_upload_size']

log = get_logger(__name__)

'''
upload_dir = 'upload_file/'
if not os.path.exists(upload_dir):
    os.makedirs(upload_dir)
'''

app = Flask(__name__)
CORS(app)

check_status = {}
 
def create_new_folder(dir_name):

    create_path = f'train_data/{dir_name}'
    # Create target Directory if don't exist
    if not os.path.exists(create_path):
        os.makedirs(create_path)
        print("Directory " , dir_name ,  " Created ")
    else:    
        print("Directory " , dir_name ,  " already exists")
        raise NameError(f'Directory /{dir_name} already exists')

def word_embeder(stopword, sample_text, folder_name):
        
    #--1--
    starttime = time.time() # to measure the processing time needed for this project
    #Get un-processed word
    words = read_data(stopword, sample_text)
    print('Data size', len(words))

    #--2--
    vocabulary_size = len(words) 
    data, dictionary, reverse_dictionary = build_dataset(words, vocabulary_size)
    del words  #delete un-preprocessed word and keep the clean curpus
    #print('Most common words (+UNK)', count[:5])
    #print('Sample data', data[:10], [reverse_dictionary[i] for i in data[:10]])

    #--3--
    batch_size=8
    num_skips=2
    skip_window=1
    batch, labels = generate_batch(batch_size, num_skips, skip_window, data)
    for i in range(8):
        print(batch[i], reverse_dictionary[batch[i]],'->', labels[i, 0], reverse_dictionary[labels[i, 0]])

    #--4-- --5--
    valid_size = 1      #切记这个数字要和len(valid_word)对应，要不然会报错哦   
    #valid_window = 2
    #验证集
    #valid_word = ["寇仲",  "孤儿" ,  "高手"]
    valid_word = ["高手"]
    valid_examples =[dictionary[li] for li in valid_word]
    graph = tf.Graph()
    vocabulary_size = len(list(set(dictionary)))
    final_embeddings = build_model(graph, valid_size, valid_examples, vocabulary_size, reverse_dictionary, data, folder_name)

    #--6--
    try:
        #to show chinese wording
        font = FontProperties(fname=r"SIMSUN.TTC", size=14)
        tsne = TSNE(perplexity=30, n_components=2, init='pca', n_iter=5000)
        plot_only = 500 #1000
        low_dim_embs = tsne.fit_transform(final_embeddings[:plot_only, :])
        labels = [reverse_dictionary[i] for i in xrange(plot_only)]
        plot_with_labels(low_dim_embs, labels, final_embeddings, folder_name, fonts=font)
        print("time used for performing this proeject: ", time.time() - starttime, "seconds")
    except ImportError:
        print("Please install sklearn, matplotlib, and scipy to visualize embeddings.")

    check_status[f'status_{folder_name}'] = True

        
@app.route("/create", methods=["POST"])
def Create():

    result = {}
    result['status'] = 1
    result['uploaded_files'] = []

    #Get and save files
    stopword = request.files.get('stopword')
    sample_text = request.files.get('sample_text')
    zip_text = request.files.get('zip_text')
    folder_name = request.args.get('name', default = 'default', type = str)

    create_new_folder(folder_name)

    log.info(f'file.filename: {stopword.filename}')
    result['uploaded_files'].append(stopword.filename)
    file_name = secure_filename('stopword_file.txt')
    stopword.save(os.path.join(f'train_data/{folder_name}', file_name))

    #User should upload either one txt file or one zip file
    if sample_text:
        log.info(f'file.filename: {sample_text.filename}')
        result['uploaded_files'].append(sample_text.filename)
        file_name = secure_filename('text_file.txt')
        sample_text.save(os.path.join(f'train_data/{folder_name}', file_name))

    else:
        log.info(f'file.filename: {zip_text.filename}')
        result['uploaded_files'].append(zip_text.filename)
        file_name = secure_filename('text_file.zip')
        zip_text.save(os.path.join(f'train_data/{folder_name}', file_name))

        #Unzip txt files and write all text into sample_text.txt
        with ZipFile(f'train_data/{folder_name}/text_file.zip', 'r') as zipObj:
        # Extract all the contents of zip file in different directory
            zipObj.extractall(f'train_data/{folder_name}/temp')
            zipObj.close()

        filenames = []

        for file in os.listdir(f'train_data/{folder_name}/temp'):
            if file.endswith(".txt"):
                filenames.append(file)

        with open(f'train_data/{folder_name}/text_file.txt', 'w+') as outfile:
            for fname in filenames:
                with open(f'train_data/{folder_name}/temp/{fname}') as infile:
                    data = infile.read()
                    outfile.write(data)
        outfile.close() 

    stopword = f'train_data/{folder_name}/stopword_file.txt'
    sample_text = f'train_data/{folder_name}/text_file.txt'

    check_status[f'status_{folder_name}'] = False

    #thread = Thread(target=word_embeder, args=(stopword, sample_text, folder_name))
    #thread.start()
    word_embeder(stopword, sample_text, folder_name)

    return jsonify(result)


@app.route("/get_result", methods=["GET"])
def Get_result():

    result = {}
    result['status'] = 1
    result['result_status'] = []

    folder_name = request.args.get('name', default = 'default', type = str)
    print(folder_name)
    print(check_status)
    try:
        if (check_status[f'status_{folder_name}'] == True): 
            return send_from_directory(directory=f'train_data/{folder_name}', filename=f'tsne.png')
        else:
            result['result_status'] = "NOT FINISHED OR NOT EXIST"
            return jsonify(result)
    except:
        result['result_status'] = "NOT FINISHED OR NOT EXIST"
        return jsonify(result)



@app.route("/get_folder_list", methods=["GET"])
def Get_folder_list():

    result = {}
    result['status'] = 1
    result['folder_name'] = []

    files = os.listdir('train_data')
    for name in files:
        if not(name.startswith('.')):
            result['folder_name'].append(name)

    print(result['folder_name'])

    return jsonify(result)




@app.route("/test", methods=["GET"])
def Test():

    result = {}
    result['status'] = 1
    result['text'] = ['OK']

    return jsonify(result)



 
@app.errorhandler(404)
def error_404(error):
    log.error(f'error: {error}')
    response = {'status':0, 'message':str(error)}
    return json.dumps(response, indent=4, ensure_ascii=False), 404

@app.errorhandler(Exception)
def error_500(error):
    log.error(error)
    response = {'status':0, 'message':str(error)}
    return json.dumps(response, indent=4, ensure_ascii=False), 500
    


if __name__ == '__main__':
    #app.run(host="0.0.0.0", port=port, debug=True)
    log.info(f"Start Service at port: {port}")
    #app.run(host="0.0.0.0", port=port, debug=True)
    serve(app, host='0.0.0.0', port=port, threads=threads, cleanup_interval=cleanup_interval, channel_timeout=channel_timeout)
